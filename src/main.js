import Vue from 'vue'
import App from './App.vue'
// VueSession
import VueSession from 'vue-session'
Vue.use(VueSession, {persist: true})
// jquery
import jQuery from 'jquery';
Vue.prototype.$$ = Vue.prototype.$jQuery = jQuery;
import 'popper.js';
import 'bootstrap';
// vuex
import store from './store'
// fontawsome
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'
// v-form
import { Form, HasError, AlertError } from 'vform'
Vue.prototype.$Form = Form
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)

// sweetalert2 start
import Swal from 'sweetalert2'
Vue.prototype.$Swal = Swal
const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
              })
Vue.prototype.$Toast = Toast

// vue router
import VueRouter from 'vue-router'
Vue.use(VueRouter)

const routes = [
  { path: '/', redirect: '/dashboard'},
  { path: '/dashboard', name: 'showDashboard', component: require('./components/Pages/Dashboard.vue').default },
  { path: '/index', name: 'showIndex', component: require('./components/Pages/Index.vue').default },
  { path: '/profile', name: 'showProfile', component: require('./components/Pages/Profile.vue').default },
  { path: '/devices', name: 'showDevices', component: require('./components/Pages/Devices.vue').default },
  { path: '/surveys', name: 'showSurveys', component: require('./components/Pages/Surveys.vue').default },
  { path: '/surveyDetail/:id', name: 'showSurveyDetail', component: require('./components/Pages/SurveyDetail.vue').default },
  { path: '/users', name: 'showUsers', component: require('./components/Pages/Users.vue').default },
  { path: '/userDetail/:id', name: 'showUserDetail', component: require('./components/Pages/UserDetail.vue').default },
  { path: '/roles', name: 'showRoles', component: require('./components/Pages/Roles.vue').default },
  { path: '/addUserToRole/:name', name: 'showAddUserToRole', component: require('./components/Pages/AddUserToRole.vue').default },
  { path: '/login', name: 'showLogin', component: require('./components/Pages/Login.vue').default },
  { path: '/register', name: 'showRegister', component: require('./components/Pages/Register.vue').default },
  { path: '/surveyCreator', name: 'showSurveyCreator', component: require('./components/Pages/SurveyCreator.vue').default },
  { path: '/survey/edit/:id', name: 'editSurveyDetail', component: require('./components/Pages/EditSurvey.vue').default },
  { path: '/api/checkForResetLink', name: 'resetPassword', component: require('./components/Pages/ResetPassword.vue').default }
]

const router = new VueRouter({
  routes,
  mode: 'history'
})

Vue.config.productionTip = false

// components
Vue.component('Sidenav', require('./components/Includes/Navigation/Sidenav.vue').default);
Vue.component('Topnav', require('./components/Includes/Navigation/Topnav.vue').default);

// global variable
Vue.prototype.$routeName

new Vue({
  render: h => h(App),
  store,
  router
}).$mount('#app')
