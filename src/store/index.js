import Vuex from 'vuex';
import Vue from 'vue';
import auth from './modules/auth';
import user from './modules/user';
import role from './modules/role';
import device from './modules/device';
import tokenInterceptor from './modules/interceptor';

tokenInterceptor()

// load vuex
Vue.use(Vuex);

// create store
export default new Vuex.Store({
    modules: {
        auth: auth,
        user: user,
        role: role,
        device: device
    }
});
