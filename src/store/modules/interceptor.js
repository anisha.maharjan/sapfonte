import axios from 'axios'

export default function tokenRefresh() {
    axios.interceptors.response.use(
        function (response) {
            return response;
        },
        function (error) {
            if(error.response.status==401){
                const tokenData= {"refreshToken": localStorage.getItem('refreshToken'),
                                    "device_id" : localStorage.getItem('device_id')}
                axios.post('http://ec2-54-89-84-11.compute-1.amazonaws.com:3000/api/user/token',tokenData)
                .then((response) => {
                    localStorage.setItem("accessToken", response.data.token)
                })
            }
        }
    );
}