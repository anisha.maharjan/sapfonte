import axios from 'axios'
const namespaced = true

const state = {
    roles : [],
    role: {},
    users:[]
};

const getters = {
    roles: (state) => state.roles,
    users: (state) => state.users
};

const actions = {
    async allRole({commit}){
        await axios.get('http://ec2-54-89-84-11.compute-1.amazonaws.com:3000/api/roles',
        {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
            }
        })
        .then((response) => {
            commit('get_roles', response.data)
        })
    },
    async addRole({commit}, role){
        await axios.post('http://ec2-54-89-84-11.compute-1.amazonaws.com:3000/api/role/add', role,
        {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
            }
        })
        commit('get_role')
    },
    async editRole({dispatch}, role){
        await axios.put(`http://ec2-54-89-84-11.compute-1.amazonaws.com:3000/api/role/edit/${role.id}`, role, 
        {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
            }
        })
        .then(()=> {
            dispatch('allRole')
        })
    },
    async deleteRole({dispatch}, id){
        await axios.delete(`http://ec2-54-89-84-11.compute-1.amazonaws.com:3000/api/role/delete/${id}`, 
        {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
            }
        })
        .then(()=> {
            dispatch('allRole')
        })
    },
    async addUserToRole({dispatch},data){
        await axios.put(`http://ec2-54-89-84-11.compute-1.amazonaws.com:3000/api/role/addUser`, data,
        {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
            }
        })
        .then(()=> {
            dispatch('allRole')
        })
    },
    async getUsers({commit}, role){
        await axios.get(`http://ec2-54-89-84-11.compute-1.amazonaws.com:3000/api/getUsers/${role}`,
        {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
            }
        })
        .then((response) => {
            commit('get_users', response.data)
        })
    }
};

const mutations = {
    get_roles: (state, roles) => {
        state.roles = roles.data
    },
    get_role: (state) => {
        state.role= 1
    },
    get_users: (state, users) => {
        state.users = users.data
    }
};

export default {
    state,
    getters,
    actions,
    mutations,
    namespaced
};