import axios from 'axios'
const namespaced = true

const state = {
    stats: [],
    user: {},
    users: [],
    activeUsers: [],
    inactiveUsers:[]
};

const getters = {
    users: (state) => state.users,
    user: (state) => state.user,
    stats: (state) => state.stats
};

const actions = {
    async stats({commit}){
        await axios.get('http://ec2-54-89-84-11.compute-1.amazonaws.com:3000/api/stats ')
        .then((response) => {
            commit('stats', response.data)
        })
    },
    async all_user({commit}){
         await axios.get('http://ec2-54-89-84-11.compute-1.amazonaws.com:3000/api/users',
        {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
            }
        })
        .then((response) => {
            commit('get_users', response.data)
        })
    },
    async activeusers({commit}){
        await axios.get('http://ec2-54-89-84-11.compute-1.amazonaws.com:3000/api/users/active ',
        {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
            }
        })
        .then((response) => {
            commit('activeUsers', response.data)
        })
    },
    async inactiveusers({commit}){
        await axios.get('http://ec2-54-89-84-11.compute-1.amazonaws.com:3000/api/users/deactive ',
        {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
            }
        })
        .then((response) => {
            commit('inactiveusers', response.data)
        })
    },
    async userDetail({commit}, id){
        await axios.get(`http://ec2-54-89-84-11.compute-1.amazonaws.com:3000/api/user/${id}`,
        {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
            }
        })
        .then((response) => {
            commit('userdetail', response.data)
        })
    },
    async addUser({dispatch},user){
        await axios.post('http://ec2-54-89-84-11.compute-1.amazonaws.com:3000/api/signup', user)
        .then(()=> {
            dispatch('all_user')
        })
    },
    async editUser({commit}, user){
        await axios.put(`http://ec2-54-89-84-11.compute-1.amazonaws.com:3000/api/user/${user.id}`, user, 
        {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
            }
        })
        .then((response) => {
            commit('userdetail', response.data)
        })
    },
    async deleteUser({dispatch}, id){
        await axios.delete(`http://ec2-54-89-84-11.compute-1.amazonaws.com:3000/api/user/${id}`, 
        {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
            }
        })
        .then(() => {
            dispatch('all_user')
        })
    },
    async deactivateUser({dispatch}, id){
        await axios.put(`http://ec2-54-89-84-11.compute-1.amazonaws.com:3000/api/user/deactivate/${id}`, id,
        {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
            }
        })
        .then(() => {
            dispatch('all_user')
        })
    },
};

const mutations = {
    get_users: (state, users) => {
        state.users = users.data
    },
    activeUsers: (state, users) => {
        state.activeUsers = users.data
    },
    inactiveusers: (state, users) => {
        state.inactiveUsers = users.data
    },
    userdetail: (state, user) => {
        state.user = user.data
    },
    stats: (state, stat) => {
        state.stats = stat
    }
};

export default {
    state,
    getters,
    actions,
    mutations,
    namespaced
};