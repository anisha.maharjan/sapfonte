import axios from 'axios'
const namespaced = true

const state = {
    user_devices :[]
};

const getters = {
    user_devices: (state) => state.user_devices,
};

const actions = {
    async userDevice({commit}, username){
        await axios.get(`http://ec2-54-89-84-11.compute-1.amazonaws.com:3000/api/devices/${username}`,
        {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
            }
        })
        .then((response) => {
            commit('get_user_devices', response.data)
        })
    },
};

const mutations = {
    get_user_devices: (state, devices) => {
        state.user_devices = devices.data
    },
};

export default {
    state,
    getters,
    actions,
    mutations,
    namespaced
};