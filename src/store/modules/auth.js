import axios from 'axios'
const namespaced = true

const state = {
    totp_time : '',
    tokenData: {}
};

const getters = {
    totp_time: state => state.totp_time
};

const actions = {
    async user_login({commit}, user){
        const response = await axios.post('http://ec2-54-89-84-11.compute-1.amazonaws.com:3000/api/login', user)

        commit('totp_time', response.data)
    },
    async user_register({commit}, user){
        const response = await axios.post('http://ec2-54-89-84-11.compute-1.amazonaws.com:3000/api/signup', user)

        commit('register', response.data)
    },
    async validate({commit}, data){
        const response = await axios.post('http://ec2-54-89-84-11.compute-1.amazonaws.com:3000/api/login/validate', data)

        commit('login', response.data)
    },
    async sendResetPasswordLink(state, email){
        await axios.post('http://ec2-54-89-84-11.compute-1.amazonaws.com:3000/api/forgotPassword', email)
    },
    async resetPassword(state, data){
        console.log(data)
        await axios.post('http://ec2-54-89-84-11.compute-1.amazonaws.com:3000/api/newPassword', data)
    }
};

const mutations = {
    login: (state, user) => {
        localStorage.setItem("username", user.data.username)
        localStorage.setItem("role", JSON.stringify(user.data.role))
        localStorage.setItem("accessToken", user.accessToken);
        localStorage.setItem("refreshToken", user.refreshToken);
        localStorage.setItem("user_id", user.data.user_id);
        localStorage.setItem("gravatar", user.data.gravatar);
        localStorage.setItem("fullName", user.data.firstName+" "+user.data.lastName)
    },
    totp_time: (state, totp) => {
        state.totp_time = totp.expireTimeInSeconds
    }
};

export default {
    state,
    getters,
    actions,
    mutations,
    namespaced
};
